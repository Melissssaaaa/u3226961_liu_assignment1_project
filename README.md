# U3226961_Liu_assignment1PartAB.ipynb

 [U3226961_Liu_assignment1PartAB.ipynb](https://gitlab.com/Melissssaaaa/u3226961_liu_assignment1_project/-/blob/main/U3226961_Liu_assignment1PartAB.ipynb) is a Python file for dealing with restaurants success by the predictive model.

## Contributing
Pull requests are welcome. 
Use the command below, steps to pull the branch from remote to local:

```bash
git checkout master
```
```bash
git pull
```

## Motivation
Hands-on practice to learn a version control tool.

To deploy my predictive modelling project to Gitlab.

## What problem does it solve

Track the changes that are made on these files. 

Create a log of any changes to these files.

## What did I learn?
Git flow: working directory --> add --> staging index --> commit--> repository
The way to initiate Git, add files to the repository, commit changes, build a public repository in the GitLab, and steps to push from local to remote.


## The hands on practice on commands in terminal.
```git
# code block
# Initialize Git
cd ASGN-11523
git init
git status

# Adding files to the repository
git status
git add .
git status

# Committing changes
git commit
git status
git add stg.txt
git status

# Steps to push from local to remote
$ git remote add origin https://gitlab.com/Melissssaaaa/u3226961_liu_assignment1_project.git
$ git commit -m "initial commit"
$ git push origin master
$ git push --set-upstream origin master
```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
